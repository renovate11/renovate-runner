module.exports = {
  endpoint: 'https://gitlab.com/api/v4/',
  platform: 'gitlab',
  persistRepoData: true,
  logLevel: 'debug',
  onboardingConfig: {
    extends: ['renovate/renovate-config'], // reference to config project that we created
  },
  autodiscover: true,
};
